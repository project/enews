; Drush Make (http://drupal.org/project/drush_make)
;
;NB: I'm not sure if this actually works yet or not -- I don't use drush make
;

api = 2
core = 7.x

; Emogrifier
libraries[emogrifier][download][type] = "get"
libraries[emogrifier][download][url] = "http://www.pelagodesign.com/emogrifier/emogrifier.zip"
