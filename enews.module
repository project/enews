<?php

/**
 * hook_perm implementation
 */
function enews_permission() {
  return array(
    'export newsletter' => array(
      'title' => t('Export Node as Basic E-news'),
      'restrict access' => TRUE,
      ),
    'theme newsletter' => array(
      'title' => t('Access Basic E-news'),
      'description' => t('Allow user to view an exported node that is hosted on the website.'),
      ),
    );
}

/**
 * hook_menu implementation
 */
function enews_menu() {
  // Export newsletter links.
  $items['node/%node/enews-export/theme'] = array(
    'title' => t('Preview Basic E-news'),
    'description' => t('View newsletter HTML before it is emogrified.'),
    'page callback' => 'enews_devel_theme',
    'page arguments' => array(1),
    'access arguments' => array('theme newsletter'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    );
  $items['node/%node/enews-export'] = array(
    'title' => t('Newsletter'),
    'page callback' => 'enews_export',
    'page arguments' => array(1,3),
    'access callback' => '_enews_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    );
  $items['node/%node/enews-export/html'] = array(
    'title' => t('Export as HTML'),
    'page callback' => 'enews_export',
    'page arguments' => array(1,3),
    'access callback' => '_enews_access',
    'access arguments' => array(1),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    );
  $items['node/%node/enews-export/html/preview'] = array(
    'title' => t('Export as HTML'),
    'page callback' => 'enews_export_preview',
    'page arguments' => array(1),
    'access callback' => '_enews_access',
    'access arguments' => array(1),
    );
  $items['node/%node/enews-export/plain'] = array(
    'title' => t('Export as Plain Text'),
    'page callback' => 'enews_export',
    'page arguments' => array(1,3),
    'access callback' => '_enews_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    );
  return $items;
}

/**
 * hook_form_alter
 *
 * Allow administrators to specify which node types can be used as
 * newsletters.
 */
function enews_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'node_type_form') {
    $type = $form['#node_type']->type;
    $enabled = variable_get('enews_' . $type, FALSE);
    $form['enews_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Newsletter'),
      '#collapsible' => TRUE,
      '#group' => 'additional_settings',
      );
    $form['enews_fields']['enews'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add a check to this box if this node type can be used as
                     a newsletter.'),
      '#return_value' => 1,
      '#default_value' => $enabled,
      );
    $form['enews_fields']['enews_css'] = array(
      '#type' => 'textarea',
      '#title' => t('Newsletter template CSS'),
      '#description' => t('Add raw CSS for this newsletter template.'),
      '#default_value' => variable_get('enews_css_' . $type, NULL),
      );

    $site_css = drupal_add_css();

    $site_css = array_keys($site_css);
    $site_css = array_combine($site_css, $site_css);

    // The global theme's stylesheets have not been added yet.
    // Add them manually.
    global $theme;
    drupal_theme_initialize();
    $theme_info = list_themes();
    $active_theme = $theme_info[$theme];
    if (!empty($active_theme->info['stylesheets'])) {
      $active_theme_css = array();
      foreach($active_theme->info['stylesheets'] as $css) {
        $active_theme_css += array_values($css);
      }
      $site_css += array_combine($active_theme_css, $active_theme_css);
    }
    

    $incl_css = variable_get('enews_advanced_css_incl_' . $type, array());
    $incl_addl_css = variable_get('enews_advanced_css_incl_addl_' . $type, "");
    $advanced = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed' => empty($incl_css),
      );
    $advanced['enews_advanced_css_incl'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Include the following stylesheets for this newsletter',
      '#options' => $site_css,
      '#default_value' => $incl_css,
      );
    $advanced['enews_advanced_css_incl_addl'] = array(
      '#type' => 'textarea',
      '#title' => 'Include additional stylesheets for this newsletter',
      '#description' => 'Enter the paths to stylesheets you wish to include, one per line, relative to the drupal root. E.g. "sites/all/themes/zen/zen.css"',
      '#default_value' => $incl_addl_css,
      );
      
    $form['enews_fields']['enews_advanced'] = $advanced;
    $form['#sumbmit'][] = 'menu_cache_clear_all';
  }
}

/**
 * enews_export
 *
 * Create an "exportable" version of a module for a mailer, both html
 * and plain text versions.
 *
 * @param object
 *  a node object
 * @param string
 *  html or plain
 */
function enews_export($node, $type = 'html') {
  $form = array();
  switch ($type) {
    case 'plain':
      $form['newsletter'] = array(
        '#type' => 'textarea',
        '#value' => drupal_html_to_text(theme('enews_emogrify', array('node' => $node))),
        '#description' => t('Select and copy the contents of this field for your third-party
                             newsletter mailer (VerticalResponse, Constant Contact, Convio, etc...)')
        );
      break;
    default :
      $options = array(
        'attributes' => array(
          'class' => 'thickbox',
          ),
        );
      $preview_link = 'node/' . $node->nid . '/enews-export/html/preview';
      $form['preview'] = array(
        '#type' => 'item',
        '#value' => l('Preview Newsletter', $preview_link, $options),
        );
      $form['newsletter'] = array(
        '#type' => 'textarea',
        '#value' => theme('enews_emogrify', array('node' => $node)),
        '#description' => t('Select and copy the contents of this field for your third-party
                             newsletter mailer (VerticalResponse, Constant Contact, Convio, etc...).')
        );
  }

  return drupal_render($form);
}

/**
 * Provide a menu callback so that admins can preview a newsletter.  This
 * page will be displayed using thickbox when it is installed.
 *
 * @param object
 *  Drupal node object.
 */
function enews_export_preview($node) {
  print theme('enews_emogrify', array('node' => $node));
}

/**
 * _enews_title
 *
 * Title callback for menu items.
 */
function _enews_title($node) {
  $title = 'Export ' . $node->title;
  return t($title);
}

/**
 * _enews_access
 *
 * Access callback for enews.
 */
function _enews_access($node) {
  $enews = variable_get('enews_' . $node->type, 0);
  if (user_access('export newsletter') AND !empty($enews)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * hook_theme implementation
 *
 * Create two presentation layer functions.  One is private, and
 * generates a template file, which is later emogrified.
 */
function enews_theme($existing, $type, $theme, $path) {
  return array(
    'enews' => array(
      'variables' => array('node' => NULL),
      'template' => 'enews',
      ),
    'enews_emogrify' => array(
      'variables' => array('node' => NULL)
      ),
    );
}

/**
 * theme_enews
 *
 * Merge module and theme CSS with custom CSS provided by this module,
 * and inline it using emogrifier.
 *
 * @see http://www.pelagodesign.com/sidecar/emogrifier/
 * @param object
 *  drupal node to be rendered as a newsletter
 * @param string
 *  emogrified HTML
 */
function theme_enews_emogrify($variables) {
  $node = $variables['node'];
  // Convert style sheets to inline styles
  $html = theme('enews', array('node' => $node));
  if (!class_exists('Emogrifier')) {
    if (module_exists('libraries')
    && $path = libraries_get_path('emogrifier')) {
      include_once($path . '/emogrifier.php');
    }
    if (!class_exists('Emogrifier')) {
      // We want this to fail if Emogrifier is still not found.
      $path = drupal_get_path('module', 'enews');
      require_once($path . '/emogrifier/emogrifier.php');
    }
  }

  // Retrieve CSS from modules and active theme.
  $css_styles = drupal_add_css();
  $incl_css = variable_get('enews_advanced_css_incl_' . $node->type, array());
  $styles = array();
  foreach($incl_css as $incl) {
    $styles[$incl] = $incl;
  }
  $incl_addl_css = variable_get('enews_advanced_css_incl_addl_' . $node->type, "");
  $lines = explode("\n", $incl_addl_css);

  foreach($lines as $line) {
    $root = $_SERVER['DOCUMENT_ROOT'];
    if(file_exists($line)) {
      $styles[$line] = $line;
    }
  }

  _enews_build_css($styles, $css);

  // Retrieve node type specific css.
  $css .= variable_get('enews_css_' . $node->type, NULL);
  // Sometimes XPATH complains about nothing.  Remove '@' signs if
  // emogrifier is not working.
  @$inliner = new Emogrifier($html, $css);
  @$content = $inliner->emogrify();

  // Convert all relative URLs to Absolute URLs
  return (!empty($content))
    ? _enews_absolute_urls($content)
    : _enews_absolute_urls($html);
}

/**
 * hook_preprocess_hook implementation
 *
 * Set up variables for newsletter template, and allow administrators to
 * create overrides for different node types.
 *
 * @param array
 *  Drupal template variable array.
 */
function template_preprocess_enews(&$vars) {
  global $base_url;
  // Include basic page regions, make sure that all links are absolute links.
  // For some reason, Drupal 7 got rid of theme_blocks. So, now we have to
  // recreate the same function manually. Thanks, Drupal.
  foreach (array('header', 'footer', 'closure') as $region) {
    $content = '';
    $build = block_get_blocks_by_region($region);
    foreach ($build as $block) {
      $content = theme('block', array('block' => $block));
    }
    $vars[$region] = _enews_absolute_urls($content);
  }

  // Load newsletter content and convert relative links to absolute links.
  // Invoking node_load is necessary here as the node object will be incomplete
  // during the 'update' operation of the nodeapi.   
  $node = $vars['node'];
  $node = node_load($node->nid, NULL, TRUE);
  $content = drupal_render(node_view($node));
  $vars['content'] = _enews_absolute_urls($content);
  $vars['title'] = $node->title;
  $vars['site_name'] = variable_get('site_name', NULL);
  // Provide template suggestions per node type.
  $vars['template_files'][] = 'enews-' . $node->type;
}

/**
 * _enews_build_css
 *
 * Read all styles sheets into a variable so that they can be inlined.
 *
 * @param array
 *  an array of stylesheets where the key is the path and the value is a
 *  boolean indicating whether or not the style should be included.
 * @param string
 *  storage variable for all styles.
 */
function _enews_build_css($styles, &$css) {
  foreach ($styles as $file => $enabled) {
    if (!empty($enabled) && file_exists($file)) {
      $css .= "\n" . file_get_contents($file);
    } 
  }
}

/**
 * _enews_absolute_urls
 *
 * Convert relative links into absolute links.
 *
 * @see http://www.howtoforge.com/forums/showpost.php?p=9&postcount=2
 * updated for CSS background images support
 *
 * @param string
 *  html string containing links
 * @param string
 *  base url
 * @return string
 *  html with absolute links.
 */
function _enews_absolute_urls($txt, $base_url_override = NULL) {
  global $base_url;
  if (!$base_url_override) {
    $base_url_override = $base_url;
  }
  $needles = array('href="', 'src="', 'background="', 'url("', "url('");
  $new_txt = '';
  if (substr($base_url_override,-1) != '/') {
    $base_url_override .= '/';
  }
  $new_base_url = $base_url_override;
  $base_url_parts = parse_url($base_url_override);

  // HTTPS breaks most auto-CDN hosting for third-party emailers.
  if ($base_url_parts['scheme'] == 'https') {
    $new_base_url = str_replace('https', 'http', $base_url_override);
    $base_url_parts['scheme'] = 'http';
  }

  foreach ($needles as $needle){
    while ($pos = strpos($txt, $needle)){
      $pos += strlen($needle);
      if (substr($txt,$pos,7) != 'http://' && substr($txt,$pos,8) != 'https://' && substr($txt,$pos,6) != 'ftp://' && substr($txt,$pos,7) != 'mailto:') {
        if (substr($txt,$pos,1) == '/') {
          $new_base_url = $base_url_parts['scheme'].'://'.$base_url_parts['host'];
        }
        $new_txt .= substr($txt,0,$pos).$new_base_url;
      }
      else {
        $new_txt .= substr($txt,0,$pos);
      }
      $txt = substr($txt,$pos);
    }
    $txt = $new_txt.$txt;
    $new_txt = '';
  }
  return $txt;
}

/**
 * Provide a callback for themers to view the newsletter markup
 * before it is emogrified.
 */
function enews_devel_theme($node) {
  global $user;
  $uid = $user->uid;
  $user = user_load(0);
  print theme('enews_emogrify', array('node' => $node));
  $user = user_load($uid);  
}
